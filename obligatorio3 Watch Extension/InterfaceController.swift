//
//  InterfaceController.swift
//  obligatorio3 Watch Extension
//
//  Created by Rodrigo de Santiago on 30/6/16.
//  Copyright © 2016 Rodrigo de Santiago. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {

    @IBOutlet var pickerValue: WKInterfacePicker!
    @IBOutlet var metricLabel: WKInterfaceLabel!
    
    var session: WCSession!
    
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        loadValuesPicker()
        pickerValue.setSelectedItemIndex(60)
        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if(WCSession.isSupported())
        {
            self.session = WCSession.defaultSession()
            self.session.delegate=self
            self.session.activateSession()
            
            sendMessageToiPhone("metric", value: "request")
        }
        
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func btnSubmitValue() {
        sendMessageToiPhone("value", value: "100")
    }
    
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
        // self.messageLabel.text= message["b"]! as String
        
        if(message.indexForKey("metric") != nil)
        {
            metricLabel.setText((message["metric"]! as! String))
        }
        
        if(message.indexForKey("result") != nil)
        {
            if message["result"] as! String == "OK"
            {
                displayAlert("Resultado", message: "Valor agregado correctamente")
            }else
            {
                displayAlert("Upps", message: "No se ha podido ingresar el valor")

            }
            
        }
    }
    
    func sendMessageToiPhone(key:String, value:String)
    {
        if(WCSession.isSupported())
        {
            session.sendMessage([key:value], replyHandler: nil, errorHandler: nil)
        }
    
    }
    
    
    func displayAlert(title:String, message:String)
    {
        let okAction = WKAlertAction(title: "OK",
                                     style: WKAlertActionStyle.Default) { () -> Void in
                                        print("OK")
        }
       
        
        presentAlertControllerWithTitle(title,
                                        message: message,
                                        preferredStyle: WKAlertControllerStyle.Alert,
                                        actions: [okAction])
    
    
    }
    
    
    func loadValuesPicker(){
        var pickerItems = [WKPickerItem]()
        
        for index in 40...400 {
            let pickerItem = WKPickerItem()
            pickerItem.title=index.description
            pickerItems.append(pickerItem)
        }
        pickerValue.setItems(pickerItems)
        
       
    }
}
