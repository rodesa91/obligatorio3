//
//  RealmManager.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 22/6/16.
//  Copyright © 2016 Romina Berrutti & Rodrigo de Santiago. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class RealmManager{
    
    
    static func saveGlucoseInput(glucoseTest:GlucoseTest)
    {
        // Get the default Realm
        let realm = try! Realm()
        
        // Persist your data easily
        try! realm.write {
            realm.add(glucoseTest)
        }
    }
    
    
    static func fetchAllGlucoseInputs() -> [GlucoseTest]
    {
        
        let controls = try! Realm().objects(GlucoseTest)
        var result = [GlucoseTest]()
        for control in controls {
           
            result.append(control)
        }
        
        return result
        
    }
    
    static func deleteGlucoseInput(glucoseTest:GlucoseTest)
    {
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(glucoseTest)
        }

    
    }
    
}