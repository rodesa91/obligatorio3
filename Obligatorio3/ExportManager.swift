//
//  ExportManager.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 15/6/16.
//  Copyright © 2016 Romina Berrutti & Rodrigo de Santiago. All rights reserved.
//

import Foundation

public class ExportManager
{

   
    
    static func createCSVfileToExport(values:[GlucoseTest]) -> String
    {
        var export:String = NSLocalizedString("Fecha/Horario,00:00 hs - 02:59 hs,03:00hs - 05:59hs,06:00 hs - 08:59 hs,09:00hs -11:59hs,12:00 hs -14:59 hs,15:00hs -17:59hs,18:00hs - 20:59hs,21:00hs - 23:59hs\n", comment: "")
        let daysDict = buildDictionary(values)
        
        for (dateKey, occurences) in daysDict {
            export+=dateKey
            for testsInHours in occurences
            {
                if testsInHours.characters.count>2
                {
                    export+=","+testsInHours.substringToIndex(testsInHours.endIndex.predecessor())
                }else{
                    export+=","+testsInHours
                }
                
            }
            
            export+="\n "
        }
            export+="\n "
            export+="Unidad:,"+Utils.getMetricString()
        
        
        return export
        
    
//        let exportFilePath = NSTemporaryDirectory() + "export.csv"
//        let exportFileURL = NSURL(fileURLWithPath: exportFilePath)
//        NSFileManager.defaultManager().createFileAtPath(exportFilePath, contents: NSData(), attributes: nil)
//      
//        var fileHandle: NSFileHandle? = nil
//        do {
//            fileHandle = try NSFileHandle(forWritingToURL: exportFileURL)
//        } catch {
//            print("Error with fileHandle")
//        }
//        
//        if fileHandle != nil {
//            fileHandle!.seekToEndOfFile()
//            
//            let csvData = export.dataUsingEncoding(NSUTF8StringEncoding)
//            fileHandle!.writeData(csvData!)
//            fileHandle!.closeFile()
//        }
//        
//        return fileHandle!
    }
    
    
    static func buildDictionary(values:[GlucoseTest]) -> [String : [String]]
    {
        var daysDict =  [String : [String]] ()
    
        var actualDay:String=""
        
        for glucoseInput in values
        {
            actualDay=(getDay(glucoseInput.time!))
            let postitionInArray:Int=getPositionInArray(glucoseInput.time!)
    
            if  !(daysDict[actualDay] == nil){
    
                var dictValue = daysDict[actualDay]
                dictValue![postitionInArray]+=" "+valueToString(glucoseInput)+" -"
                daysDict[actualDay]=dictValue
    
            }else{
                var valuesInHourRange: [String] = ["","","","","","","",""]
                valuesInHourRange[postitionInArray]+=valueToString(glucoseInput)+" -"
                daysDict[actualDay] = valuesInHourRange
            }
    
        }
    
        return daysDict
    }

    static func valueToString(glucoseInput:GlucoseTest)-> String
    {
        // Esto es chuco hay que ver que tal funciona
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Hour , .Minute], fromDate: glucoseInput.time!)
    
        let hour =  components.hour
        let minutes = components.minute
    
        let resultado = glucoseInput.glucoseLevel.description+" ("+hour.description+":"+minutes.description+")"
        return resultado
    }


    
    
    static func getPositionInArray(date:NSDate)-> Int
    {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Hour], fromDate: date)
        
        let hour =  components.hour
        
        switch hour {
        case 00,01,02:
            return 0
        case 03,04,05:
            return 1
        case 06,07,08:
            return 2
        case 09,10,11:
            return 3
        case 12,13,14:
            return 4
        case 15,16,17:
            return 5
        case 18,19,20:
            return 6
        case 21,22,23:
            return 7
            
            
        default:
            return 0
        }
        
    }
    
    
    static func getDay(date:NSDate)-> String
    {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: date)
        
        let year =  components.year
        let month = components.month
        let day = components.day
        
        // chequear preferencia de salida de dias
        return day.description+"/"+month.description+"/"+year.description
        
    }
    
    


}