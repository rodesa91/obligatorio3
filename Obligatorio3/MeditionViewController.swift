//
//  MeditionViewController.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 24/6/16.
//  Copyright © 2016 Romina Berrutti & Rodrigo de Santiago. All rights reserved.
//

import UIKit

class MeditionViewController: UIViewController {

    var allGlucoseTest:[GlucoseTest] = []
    @IBOutlet var tableGlucoseValues: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allGlucoseTest=RealmManager.fetchAllGlucoseInputs()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(animated: Bool) {
        allGlucoseTest=RealmManager.fetchAllGlucoseInputs()
        tableGlucoseValues.reloadData()
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MeditionViewController:UITableViewDelegate, UITableViewDataSource {
    
    
   

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return allGlucoseTest.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let celda:MeditionCell = self.tableGlucoseValues.dequeueReusableCellWithIdentifier("cell") as! MeditionCell
        
        celda.icon.image=UIImage(named:getIconName(allGlucoseTest[indexPath.row].glucoseLevel))
        let glucoseLevelString = Utils.createStringGlucoseLevelWithMeditionUnit(allGlucoseTest[indexPath.row].glucoseLevel)
        
        celda.glucoseValue.text = glucoseLevelString
        
        if(allGlucoseTest[indexPath.row].time?.description != nil)
        {
            celda.date.text = Utils.getStringFromDate(allGlucoseTest[indexPath.row].time!)
        }else{
            celda.date.text = "00:00 27/09/1891"
        }
        
        
        return celda
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        RealmManager.deleteGlucoseInput(allGlucoseTest[indexPath.row])
        allGlucoseTest.removeAtIndex(indexPath.row)
        self.tableGlucoseValues.reloadData()
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        actualCoupon=coupons[indexPath.row]
//        self.performSegueWithIdentifier("showCouponDescription", sender: nil)
        
    }
    
    
    
    func getIconName(valueTest:Int)->String
    {
    
        if(75>valueTest)
        {
            return "iconRedTest"
        }else{
            if(valueTest<140)
            {
                return "iconGreenTest"
            }
            return "iconBlackTest"
        }
    
    }
    
    





}
