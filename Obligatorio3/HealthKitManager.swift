//
//  HealthKitManager.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 15/6/16.
//  Copyright © 2016 Romina Berrutti & Rodrigo de Santiago. All rights reserved.
//

import Foundation
import HealthKit

public class HealthKitManager
{

   
   
   static let healthKitStore: HKHealthStore = HKHealthStore()
    
    
    static func storeValuesHealthKitEnabled()->Bool
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if(!(defaults.objectForKey("enableHealthKit")==nil)&&defaults.objectForKey("enableHealthKit") as! String == "no"){
            return false
        }
        
        return true
    }
    
    static func allowStoreValuesInHealtKit(permission:Bool)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if(permission)
        {
            defaults.setObject("yes", forKey: "enableHealthKit")
        }else{
            defaults.setObject("no", forKey: "enableHealthKit")
        }
        
    }
    
    
     static func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!)
    {
        // State the health data type(s) we want to read from HealthKit.
        let healthDataToRead = Set(arrayLiteral: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodGlucose)!)
        
        // State the health data type(s) we want to write from HealthKit.
        let healthDataToWrite = Set(arrayLiteral: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodGlucose)!)
        
        // Just in case OneHourWalker makes its way to an iPad...
        if !HKHealthStore.isHealthDataAvailable() {
            print("Can't access HealthKit.")
        }
      
        // Request authorization to read and/or write the specific data.
        healthKitStore.requestAuthorizationToShareTypes(healthDataToWrite, readTypes: healthDataToRead) { (success, error) -> Void in
            if( completion != nil ) {
                completion(success:success, error:error)
            }
        }
    }
    
    
    static func addGlucoseValue(input:GlucoseTest,completion: ((success:Bool, error:NSError!) -> Void))
    {
        

        let unitSelection = HKUnit(fromString: "mg/dl")
        let glucoseInBlood = Double(input.glucoseLevel)
        
        let glucose = HKQuantitySample(
            type: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodGlucose)!,
            quantity: HKQuantity(unit:unitSelection, doubleValue: glucoseInBlood),
            startDate: input.time!,
            endDate: input.time!,
            metadata: [:] )
     
        healthKitStore.saveObject(glucose, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print("Error saving Glucose sample: \(error!.localizedDescription)")
            } else {
                print("Glucose sample saved successfully!")
            }
        })
    }


    
}
