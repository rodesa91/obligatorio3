//
//  ExportViewController.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 27/6/16.
//  Copyright © 2016 Romina Berrutti & Rodrigo de Santiago. All rights reserved.
//

import UIKit
import MessageUI

class ExportViewController: UIViewController,MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func exportButton(sender: AnyObject) {
        
        let mailContent = NSMutableString()
        mailContent.appendString(ExportManager.createCSVfileToExport(RealmManager.fetchAllGlucoseInputs()))
        let data = mailContent.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)

        let emailViewController = configuredMailComposeViewController(data!)
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(emailViewController, animated: true, completion: nil)
        }
        
    }
    
    
    func configuredMailComposeViewController(data: NSData) -> MFMailComposeViewController {
        let emailController = MFMailComposeViewController()
        emailController.mailComposeDelegate = self
        emailController.setSubject("Valores glicemia")
        emailController.setMessageBody("", isHTML: false)
        
        emailController.addAttachmentData(data, mimeType: "text/csv", fileName: "Resultados.csv")
        
        return emailController
    }
    
    
    func showSendMailErrorAlert() {
//        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
//        sendMailErrorAlert.show()
        
        let alert = UIAlertController(title: "Error al enviar mail", message:"El dispositivo no tiene el correo configurado. Revisar configuracion de correo electronico en Configuracion del iPhone y reintentar", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
        self.presentViewController(alert, animated: true){}
        
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
