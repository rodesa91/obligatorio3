//
//  Utils.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 17/6/16.
//  Copyright © 2016 Romina Berrutti &  Rodrigo de Santiago. All rights reserved.
//

import UIKit

public class Utils
{

    static func displayWarning(title:String,mainContent:String,buttonContent:String)-> Void
    {
        let alert = UIAlertView()
        alert.title = title
        alert.message = mainContent
        alert.addButtonWithTitle(buttonContent)
        alert.show()
        
    }
    
    static func createStringGlucoseLevelWithMeditionUnit(GlucoseValue:Int) ->String
    {
        if(useStandardMetric())
        {
            return GlucoseValue.description+" mg/dl"
        }
        
        var value = mmolCalculator(GlucoseValue).description
        
        if value.characters.count>4
        {
            value = value.substringToIndex(value.startIndex.advancedBy(4))
        }
        
        return value+" mmol/L"
    }
    
    
    static func mmolCalculator(value:Int) ->Double
    {
        return Double(value)/18
    
    }
    
    
    
    static func useStandardMetric()->Bool
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if(!(defaults.objectForKey("glucoseMetric")==nil)&&defaults.objectForKey("glucoseMetric") as! String == "mmol/L"){
            return false
        }
        
        return true
    }
    
    static func setUseOfStandardMetric(preference:Bool)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if(preference)
        {
            defaults.setObject("mg/dl", forKey: "glucoseMetric")
        }else{
            defaults.setObject("mmol/L", forKey: "glucoseMetric")
        }
        
    }
    
    
    static func getMetricString() -> String
    {
        if(useStandardMetric())
        {
            return "mg/dl"
        }else{
            return "mmol/L"
        }
    
    
    }
    
    static func getDateFromString(date:String) -> NSDate
    {
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        formatter.timeStyle = .ShortStyle
        return formatter.dateFromString(date)!
        
    }

    

    static func getStringFromDate(date:NSDate) -> String
    {
      
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        formatter.timeStyle = .ShortStyle
        return formatter.stringFromDate(date)
        
    }

}