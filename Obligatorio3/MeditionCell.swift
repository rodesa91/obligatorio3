//
//  MeditionCell.swift
//  Obligatorio3
//
//  Created by Rodrigo de Santiago on 23/6/16.
//  Copyright © 2016 Rodrigo de Santiago. All rights reserved.
//

import UIKit

class MeditionCell: UITableViewCell
{

    @IBOutlet var glucoseValue:UILabel!
    @IBOutlet var date:UILabel!
    @IBOutlet var icon:UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
