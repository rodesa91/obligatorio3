//
//  ViewController.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 9/6/16.
//  Copyright © 2016 Romina Berrutti & Rodrigo de Santiago. All rights reserved.
//

import UIKit
import WatchConnectivity

class MainViewController: UIViewController{

    @IBOutlet var textInputValue: UITextField!
    @IBOutlet var labelMetric: UILabel!
    @IBOutlet var dateTextField: UITextField!
    
    var session:WCSession!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        if(WCSession.isSupported())
        {
            self.session = WCSession.defaultSession()
            self.session.delegate=self
            self.session.activateSession()
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewDidAppear(animated: Bool) {
        labelMetric.text = Utils.getMetricString()
        dateTextField.text=Utils.getStringFromDate(NSDate())
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    @IBAction func btnAddMedition(sender: AnyObject) {
    
        if(textInputValue.text=="" || textInputValue.text==nil)
        {
            Utils.displayWarning("Aviso", mainContent: "Debe ingresar un valor de glucosa en sangre", buttonContent: "Ok")
        }else{
            let glucoseInput:GlucoseTest=GlucoseTest()
            glucoseInput.glucoseLevel=Int(textInputValue.text!)!
            glucoseInput.time=Utils.getDateFromString(dateTextField.text!)
            RealmManager.saveGlucoseInput(glucoseInput)
            if(HealthKitManager.storeValuesHealthKitEnabled())
            {
                authorizeHealthKit()
                HealthKitManager.addGlucoseValue(glucoseInput, completion: { (success, error) in
                    if success
                    {
        
                    }else{
                        Utils.displayWarning("Aviso", mainContent: "Error al guardar en health kit", buttonContent: "Ok")
                    }
                    }
                )
            
            }
            
            clearData()
            Utils.displayWarning("", mainContent: "Se ha ingresado el valor correctamente", buttonContent: "Ok")
        }
       
        
    
    }
    
    @IBAction func dateEditing(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.DateAndTime
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(MainViewController.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        dateTextField.text = Utils.getStringFromDate(sender.date)
        
    }
    
    
    func authorizeHealthKit()
    {
        HealthKitManager.authorizeHealthKit{ (authorized,  error) -> Void in
            if authorized {
                print("HealthKit authorization received.")
            }
            else
            {
                print("HealthKit authorization denied!")
                if error != nil {
                    print("\(error)")
                }
            }
        }
    }
    
    
    func controlValidInput() -> Bool {
        if let text = textInputValue.text where !text.isEmpty
        {
            return true
        }
        return false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func clearData()
    {
        textInputValue.text=""
    }
    
    

}


extension MainViewController:WCSessionDelegate
{
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
        // self.messageLabel.text= message["b"]! as String
        
        if(message.indexForKey("metric") != nil)
        {
            sendMessageToWatch("metric", value: Utils.getMetricString())
        }
        
        if(message.indexForKey("value") != nil)
        {
            let value:Int?=Int(message["value"] as! String)
            let glucoseInput:GlucoseTest = GlucoseTest()
            glucoseInput.glucoseLevel=value!
            glucoseInput.time = NSDate()
            RealmManager.saveGlucoseInput(glucoseInput)
            if(HealthKitManager.storeValuesHealthKitEnabled())
            {
                authorizeHealthKit()
                HealthKitManager.addGlucoseValue(glucoseInput, completion: { (success, error) in
                    if success
                    {
                        self.sendMessageToWatch("result", value: "OK")
                        
                    }else{
                       self.sendMessageToWatch("result", value: "Error")
                    }
                    }
                )
                
            }else{
                 self.sendMessageToWatch("result", value: "OK")
            }
            
            
        }
    }
    
    func sendMessageToWatch(key:String, value:String)
    {
        if(WCSession.isSupported())
        {
            session.sendMessage([key:value], replyHandler: nil, errorHandler: nil)
        }
        
    }

}

