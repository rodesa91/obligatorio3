//
//  OptionsViewController.swift
//  Obligatorio3
//
//  Created by Romina Berrutti & Rodrigo de Santiago on 26/6/16.
//  Copyright © 2016 Romina Berrutti & Rodrigo de Santiago. All rights reserved.
//

import UIKit

class OptionsViewController: UITableViewController {

    @IBOutlet var metricSegmentControl: UISegmentedControl!
    
    @IBOutlet var enableHealthKitCommunicationSwitch: UISwitch!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if !Utils.useStandardMetric() {
            metricSegmentControl.selectedSegmentIndex=1
        }
        
        if !HealthKitManager.storeValuesHealthKitEnabled()
        {
            enableHealthKitCommunicationSwitch.setOn(false, animated: false)
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func metricSegmentControlChange(sender: AnyObject) {
        
        switch metricSegmentControl.selectedSegmentIndex {
        case 0:
            Utils.setUseOfStandardMetric(true)
        case 1:
            Utils.setUseOfStandardMetric(false)
        default:
            Utils.setUseOfStandardMetric(true)
        }

    }

    @IBAction func healthKitSwitchChange(sender: AnyObject) {
        
        if enableHealthKitCommunicationSwitch.on {
            HealthKitManager.allowStoreValuesInHealtKit(true)
        }else{
            HealthKitManager.allowStoreValuesInHealtKit(false)
        }
    }
   
}
